

// jQuery(function($){
//   $('#example').DataTable();
// });
$ = jQuery;
$(document).ready(function() {

  $('#example').DataTable({

    searching: false,
    aLengthMenu: [[5, 10, -1], [5, 10, "All"]],
    order: [[ 1, "desc" ]]
  });
} );
