<?php  /*Template Name:Custom */?>


<?php

get_header(); ?>


<script type="text/javascript" src="<?php echo plugins_url('/scripts/datatable.js'); ?>"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		<?php
		global $wpdb;
		$tablename = $wpdb->prefix.'posts';
		 $query = "select * from $tablename where post_type='post' and post_status!='trash'";
		 $posts = $wpdb->get_results($query);
		 $tags_array = array();
			// print_r($posts);
		 foreach ($posts as $post) {
			 // code...
			 if(get_the_tags($post->ID)){
				 foreach (get_the_tags($post->ID) as $key ) {
					 $array[$key->slug] = $post->ID;
				 }
			 }
		 }
		 // print_r($array);
		 if(empty($array)) return;
		 foreach (array_keys($array) as $key) {
			 $tags_array[$key] = 0;
		 }


		 foreach ($posts as $key) {
			 $tag_arr = get_the_tags($key->ID);
			 if(is_array($tag_arr) || is_object($tag_arr)){
				 foreach ($tag_arr as $slug ) {
					 if(in_array($slug->slug, $tags_array)){
						 $count = get_post_meta($key->ID, 'test_app_vote', true);
						 $tags_array[$slug->slug] =  $tags_array[$slug->slug] +$count ;
					 }
				 }
			 }
		 }



			 echo '<table id="example" class="display" style="width:100%"><thead><th>Tag</th><th>Like</th></thead><tbody>';
			 foreach (array_keys($tags_array) as $key ) {
			 	echo "<tr><td>$key</td> <td>$tags_array[$key]</td></tr>";

			 	}
			 echo '</tbody></table>';
		?>

		</div>
	</div>
</div>

<?php
get_sidebar();
get_footer();
