<?php
/*
Plugin Name: Test
Plugin URI: -
Description: Açıklama kısmı
Version: 1.1.1
Author: K
Author URI: ~
*/

class TestApp{

    function __construct(){
        add_filter("the_content", array(&$this, 'test_app_vote_content'));
        add_action('wp_enqueue_scripts', array(&$this, 'scripts'));  //normal sayfa için load javascript!
      	add_action('admin_init', array(&$this, 'admin_init_test_app'));
        add_action('publish_post', array(&$this, 'test_app_post_meta'));
        add_action('wp_ajax_test-app-main', array(&$this, 'test_app_main'));
        add_action('wp_ajax_nopriv_test-app-main', array(&$this, 'test_app_main'));
        add_action('widgets_init', create_function('', 'register_widget("TestApp_Widget");'));
        add_action('admin_init',array(&$this, 'custom_page_tags'));
        add_filter( 'wp_head', array(&$this,'wpb_custom_new_menu' ));
        add_filter( 'page_template',  array(&$this,'wpa3396_page_template') );

    }
    function wpa3396_page_template( $page_template )
    {

        $page_template = dirname( __FILE__ ) . '/lib/custom_page.php';

        return $page_template;
    }
    function wpb_custom_new_menu() {
      echo '<h1><a href="'.get_site_url().'/index.php/etiketlere-gore-toplam-begeni/">Etiketler</a></h1>';

    }

    function admin_init_test_app(){
        register_setting( 'test-app', 'test_app_settings' ); // settings için gerekli
    }

    function test_app_post_meta($post_id){
        if(!is_numeric($post_id)) return;
        add_post_meta( $post_id, 'test_app_vote', 0, true );
    }

    function test_app_vote_content($content){
        global $post;
        $active = '';
        if(isset($_COOKIE['test_app-'.$post->ID])){
            $active = 'active';
        }
        $content .= '<p class="vote-submit-'.$active.'" id="'.$post->ID.'"><i class="fa fa-thumbs-up '.$active.'"></i></p>';
        $content .= $this->test_app_main($post->ID);
        return $content;

    }

    function test_app_main($post_id, $action='get'){

        if(isset($_POST['post_id'])){
            $action = $_POST['x'];
            $post_id = $_POST['post_id'];
        }

        switch ($action) {
            case 'get':
                $count = get_post_meta($post_id, 'test_app_vote', true);
                return '<span class="test-app-count">'. $count .'</span>';
                break;

            case 'update':
                $count = get_post_meta($post_id, 'test_app_vote', true);
                if(isset($_COOKIE['test_app-'.$post_id])) return $count;
                $count++;
                update_post_meta( $post_id,'test_app_vote', $count);
                setcookie('test_app-'.$post_id, $post_id, time()*5, '/');
                return '<span class="test-app-count">'. $count .'</span>';
                break;
            case 'dis_update':
                $count = get_post_meta($post_id, 'test_app_vote', true);
                $count--;
                unset($_COOKIE['test_app-'.$post_id]);
                setcookie('test_app-'.$post_id, null, -1, '/');
                update_post_meta( $post_id,'test_app_vote', $count);
                return '<span class="test-app-count">'. $count .'</span>';
                break;
        }
    }


    function scripts(){
        wp_enqueue_script( 'test-app-like', plugins_url( '/scripts/enable.js', __FILE__ ), array('jquery') );
        wp_enqueue_script( 'test-app-unlike', plugins_url( '/scripts/disable.js', __FILE__ ), array('jquery') );
        wp_enqueue_script( 'datatable', plugins_url( '/scripts/datatable.js', __FILE__ ), array('jquery') );
        wp_enqueue_style( 'test-css', plugins_url( '/styles/test.css', __FILE__ ) );
        wp_enqueue_style('Font_Awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');
        wp_enqueue_style('datatable-css', 'https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css');
        wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.3.1.js');
        wp_enqueue_script('data-table-js', 'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js');
        wp_localize_script( 'test-app-like', 'test_app', array('ajaxurl' => admin_url('admin-ajax.php')) );
    }

    function custom_page_tags(){
      if ( is_admin()){
        $new_page_title = 'Etiketlere göre toplam beğeni';
        $new_page_content = "1";
        $page_check = get_page_by_title($new_page_title);
        $temp =  $this->wpa3396_page_template( $page_template );
        $new_page = array(
              'post_type' => 'page',
              'post_title' => $new_page_title,
              'page_template'  => $temp,
              'post_status' => 'publish',
              'post_author' => 1,);

        if(!isset($page_check->ID)){
          $new_page_id = wp_insert_post($new_page);
        }
      }
    }


}

global $TestApp;
$TestApp = new TestApp();

class TestApp_Widget extends WP_Widget {

	public function __construct() {
		$widget_ops = array(
			'classname' => 'TestApp',
			'description' => 'En çok beğeni alan 10 yazı.',
		);
		parent::__construct( 'test_app_widget', 'TestApp', $widget_ops );
	}

	public function widget( $args, $instance ) {
		extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $posts = $instance['posts'];

        echo $before_widget;
        if( !empty( $title ) ) echo $before_title . $title . $after_title;
        $likes_posts_args = array(
			'numberposts' => $posts,
            'orderby' => 'meta_value_num',
			'order' => 'DESC',
            'meta_key' => 'test_app_vote',
			'post_type' => 'post',
			'post_status' => 'publish'
		);
        $likes_posts = get_posts($likes_posts_args);
        foreach( $likes_posts as $likes_post ) {
			//ip
			$count =get_post_meta( $likes_post->ID, 'test_app_vote', true);
			$count_output = " <span class='test-app-count'>($count)</span>";

			echo '<li><a href="' . get_permalink($likes_post->ID) . '">' . get_the_title($likes_post->ID) . '</a>' . $count_output . '</li>';
		}
        echo '</ul>';

		echo $after_widget;
	}

	public function form( $instance ) {
        $instance = wp_parse_args($instance);

        $default = array(
            'title' => 'En çok beğenilenler',
            'posts' => 10,
        );

        $instance = wp_parse_args($instance, $default);

        $title = $instance['title'];
        $posts = $instance['posts'];
        ?>
        <p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('posts'); ?>"><?php _e('Posts:'); ?></label>
			<input id="<?php echo $this->get_field_id('posts'); ?>" name="<?php echo $this->get_field_name('posts'); ?>" type="text" value="<?php echo $posts; ?>" size="3" />
		</p>
        <?php

	}

	public function update( $new_instance, $old_instance ) {
        $old_instance['title'] = strip_tags($new_instance['title']);
        $old_instance['posts'] = strip_tags($new_instance['posts']);
        return $old_instance;
	}
}
